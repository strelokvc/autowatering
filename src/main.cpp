#ifndef UNIT_TEST
#include <Arduino.h>
#endif
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>

#include <WiFiUdp.h>
#include <NTPClient.h>
#include "Ticker.h"
#include <EEPROM.h>

const char* ssid[] = {"latstrel17-1-158", "Onlime_43", "HOVERSURF 559"};
const char* pass[] = {"192168001055", "3214569870", "hoversuRF"};

const char* www_username ="admin";
const char* www_password="agava";

MDNSResponder mdns;
//byte packetBuffer[512]; //buffer to hold incoming and outgoing packets
ESP8266WebServer server(80);

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "2.ru.pool.ntp.org", 3600*3, 60000);

bool waterOn = false;
bool timerStarted = false;

void connectWifi();
void openRelay();
void closeRelay();
void handleRoot();

void handleNotFound();
void handleStation();
void handleSettingsSave();
void handleSettingsChange();

void startWatering();
void stopWatering();
void startTestWatering(int testPeriodMs);
void stopTestWatering();

void updateTimersPeriods();

void updateNtp();

String getTimeStringFromMs(int millis);

void SaveEeprom();
void LoadEeprom();

#define MDNS_NAME "watering"

#define WATERING_DEFAULT_PERIOD_MS 60*1000 //1min
#define WATERING_DEFAULT_DURATION_MS 5*1000 //5secs
#define WATERING_DEFAULT_TEST_DURATION_MS 1000

int wateringPeriodMs = WATERING_DEFAULT_PERIOD_MS;
int wateringDurationMs = WATERING_DEFAULT_DURATION_MS;
int waterintTestDurationMs = WATERING_DEFAULT_TEST_DURATION_MS;

Ticker timerWateringPeriod(startWatering, wateringPeriodMs, 0, MILLIS);
Ticker timerWateringDuration(stopWatering, wateringDurationMs, 0, MILLIS);
Ticker timerTestWateringDuration(stopTestWatering, waterintTestDurationMs, 0, MILLIS);



void setup() {
  EEPROM.begin(128);
  LoadEeprom();
  Serial.begin(9600);
  while (!Serial) ;

  connectWifi();

  timeClient.begin();
  server.on("/changeSettings", []() {
    if (!server.authenticate(www_username, www_password)) {
      return server.requestAuthentication();
    }   
    //authenticate is OK
    handleSettingsChange();

});
  server.on("/", handleRoot);  
  server.on("/station", handleStation);
  server.on ("/saveSettings", handleSettingsSave);
  server.on ("/changeSettings", handleSettingsChange);

server.on("/inline", [](){
  server.send(200, "text/plain", "this works as well");
});
server.begin();
server.onNotFound(handleNotFound);  
 
Serial.println("HTTP server started");
timerWateringPeriod.start();
  //startWatering();
}

void loop() {
  updateNtp();
  server.handleClient();
  timerWateringPeriod.update();
  timerWateringDuration.update();
  timerTestWateringDuration.update();
}

unsigned long lastTimeUpdate=0;
const unsigned long ntpUpdatePeriodMs = 1000 * 60 * 60; //1hour
void updateNtp(){
  if(!lastTimeUpdate || (lastTimeUpdate + ntpUpdatePeriodMs) < millis()) {
    timeClient.update();
    lastTimeUpdate = millis();
    Serial.println("NTP updated!");
  }
}

struct {
  uint period = 0;
  uint duration = 0;
} settingData;

void LoadEeprom(){
  EEPROM.get(0, settingData);
  if(settingData.period > 0){
    wateringPeriodMs = settingData.period;
    updateTimersPeriods();
  }
  if(settingData.duration > 0){
    wateringDurationMs = settingData.duration;
    updateTimersPeriods();
  }
}

void SaveEeprom(){
  settingData.period = wateringPeriodMs;
  settingData.duration = wateringDurationMs;
  EEPROM.put(0, settingData);
  EEPROM.commit();
}

void closeRelay(){
  Serial.write("\xa0\x01\x00\xa2"); // CLOSE RELAY
  Serial.write(0x00);
  Serial.write(0xa1);
}

void openRelay(){
  Serial.write("\xa0\x01\x01\xa2"); // OPEN RELAY
  Serial.write(0x00);
  Serial.write(0xa1);
}

void printWifiStatus() {
  String ssid = String("SSID: " + WiFi.SSID() + " [" + (WiFi.status() == WL_CONNECTED? "Connected]":"Not Connected]"));
  Serial.println(ssid);
  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
}

void connectWifi() {
  int wifiStations = sizeof(ssid)/sizeof(ssid[0]);
  bool connected = false;

  while (true)
  for(int i=0; i<wifiStations; i++){
    // setting up Station AP
    WiFi.begin(ssid[i], pass[i]);

    // Wait for connect to AP
    Serial.print("[Connecting] ");
    Serial.print(ssid[i]);
    int tries = 0;
    while (!connected) {
      delay(500);
      Serial.print(".");
      tries++;
      if (tries > 15){
        break;
      }
      connected = WiFi.status() == WL_CONNECTED;
    }

    Serial.println();

    if(!connected) continue;

    printWifiStatus();

    if (mdns.begin(MDNS_NAME, WiFi.localIP())) {
      Serial.println("MDNS responder started. Name: '" + String(MDNS_NAME) + "'");
    }
    return;
  }
}

void handleRoot() {

String scriptJQuery="<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>"
"<script>"
  "var timePeriodMs;"
  "$('#testButton').click(function(e){"
    "e.preventDefault();"
    "timePeriodMs = $('#test_period').val();"
    "$.get('/changeSettings?testWater=' + timePeriodMs"
    ", function(data){"
    "console.log(data);"
  " });  });</script>";

  String css = "<style>#formPassword {display: none;}button {margin-bottom: 10px;}"
  "div#content {background-color:rgba(255, 255, 255, 0.5); padding:10px;}</style>";

  String content = String("<html><head><title>Watering Station</title>"+ scriptJQuery  + css +"</head><body>");

  content += "<div id=\"content\"><p style=\"color:black;\";><font size = \"5\">Current time: <b>" + timeClient.getFormattedTime() + "</b></font></p>";
  content += "<p><font size = '5'>Time after last launch: <b>" + getTimeStringFromMs(timerWateringPeriod.elapsed()) + "</b></font></p>";
  content += "<p><font size = '5'>Time to next launch: <b>" + getTimeStringFromMs(timerWateringPeriod.getInterval() - timerWateringPeriod.elapsed()) + "</b></font></p>";
  content += "<p><font size = '5'>Launched times: <b>" + String(timerWateringPeriod.counter()) + "</b></font></p>";
  content += "<p><font size = '5'>Timer state: <b>" + String(timerWateringPeriod.state()) + "</b></font></p>";
  content += "<p><font size = '5'>LastTime: <b>" + getTimeStringFromMs(timerWateringPeriod.getLastTime()) + "</b></font></p>";
  //content += "<p style='color:#180BF4;';><font size = '5'>NextLaunchTime: <b>" + getTimeStringFromMs(timerWateringPeriod.getLastTime()+timerWateringPeriod.getInterval()-timerWateringPeriod.elapsed()) + "</b></font></p>";

  content += "<p><font size = '5'>Interval: <b>" + getTimeStringFromMs(timerWateringPeriod.getInterval()) + "</b></font></p>";

  content += "<p>Watering <b>PERIOD</b>: " + getTimeStringFromMs(wateringPeriodMs)+"</p>" ;//+ "<button onclick=\"window.location.href=\'/changeSettings\'\"> Set</button></p>";
  content += "<p>Watering <b>DURATION</b>: " + getTimeStringFromMs(wateringDurationMs)+"</p>"  + "<button onclick=\"window.location.href=\'/changeSettings\'\"> Change Settings</button>";

  content += "</div></body></html>";
  server.send(200, "text/html", content);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++)
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  server.send(404, "text/plain", message);
}

void handleStation() {
  for (uint8_t i = 0; i < server.args(); i++) {
    if (server.argName(i) == "waterOn") {
      openRelay();
      waterOn = true;
      handleRoot();
      //server.send(200, "text/plain", "Off");
    }
    else if(server.argName(i) == "waterOff"){
      closeRelay();
      waterOn = false;
      handleRoot();
      //server.send(200, "text/plain", "On");
    }
    else if(server.argName(i) == "testWater"){
      if(waterOn)
        server.send(500, "text/plain", "Already watering...");
      else{
        int timeMs = server.arg("testWater").toInt();
        startTestWatering(timeMs);
        server.send(200, "text/plain", String("turnedOn:" + timeMs));
      }
    }
    else
      server.send(200, "text/plain", "command not found...");

  }

  //handleRoot();
}

String jQuerymsToTime = String("<script>$(document).ready(function(){"
        "$('#period_ms').on('input', function(){ "
            "var d=msToTime($(this).val());"
            "$('#period_minsec').text(msToTime($(this).val()));"
        "});"
        "$('#duration_ms').on('input', function(){ "
            "var d=msToTime($(this).val());"
           "$('#duration_minsec').text(msToTime($(this).val()));"
        "});"
        "$('#test_period').on('input', function(){ "
            "var d=msToTime($(this).val());"
           "$('#test_minsec').text(msToTime($(this).val()));"
        "});"              
 "});"
 "function msToTime(duration) {"
  "var milliseconds = parseInt((duration % 1000) / 100),"
    "seconds = Math.floor((duration / 1000) % 60),"
    "minutes = Math.floor((duration / (1000 * 60)) % 60),"
    "hours = Math.floor((duration / (1000 * 60 * 60)) % 24);"
  "hours = (hours < 10) ? '0' + hours : hours;"
  "minutes = (minutes < 10) ? '0' + minutes : minutes;"
  "seconds = (seconds < 10) ? '0' + seconds : seconds;"
  "return hours + 'h ' + minutes + 'm ' + seconds + '.' + milliseconds;"
"}"
"</script>");

void handleSettingsChange(){
Serial.println("Enter handleRoot");
for (uint8_t i = 0; i < server.args(); i++) {
    if (server.argName(i) == "waterOn") {
      openRelay();
      waterOn = true;
      //server.send(200, "text/plain", "Off");
    }
    else if(server.argName(i) == "waterOff"){
      closeRelay();
      waterOn = false;
      //server.send(200, "text/plain", "On");
    }
}
  String header;
  char htmlResponse[3000];
  String waterLo = String(waterOn?"Off":"On");
  String htmlPageSettings = String("<!DOCTYPE html>"
  "<html lang=\"en\">"
    "<head>"
    "<meta charset=\"utf-8\">"
      "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
    "</head>"
    "<body><div id='content'>"
    "<h1>Watering immediately</h1>"
    "<a href='/changeSettings?water" + waterLo + "'>Water" + waterLo + "</a>"
    "<h1>Test of watering</h1>"
    "<p><input type='number' id='test_period' size=2 value='" + String(WATERING_DEFAULT_TEST_DURATION_MS) + "' autofocus> ms"
        "<button id='testButton'>Run</button> <label id='test_minsec'></label><br/>"
        "<h1>Relay activation period</h1>"
            "<input type='number' name='period_ms' id='period_ms' size=2 value='" + wateringPeriodMs + "' autofocus> ms  <label id='period_minsec'></label>"
            "<br>"
            "<h1>The duration of the realy</h1>"
            "<input type='number' name='duration_ms' id='duration_ms' size=2 value='" + wateringDurationMs + "' > ms <label id='duration_minsec'></label>"
            "<div>"
            "<br><button id=\"save_button\">Save</button>"
            "<br><button onclick=\"window.location.href=\'/\'\">Back</button>"
            "</div>"
      "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>"
      "<script>"
      "  var hh;"
      "  var mm;"
      "  var ss;"
      "  $('#save_button').click(function(e){"
        "  e.preventDefault();"
        "  pms = $('#period_ms').val();"
        "  dms = $('#duration_ms').val();"
        " $.get('/saveSettings?pms=' + pms + '&dms=' + dms "
        "  , function(data){"
        "    console.log(data);"
        "  });"
      "  });"
      "</script>"+jQuerymsToTime+"</div></body>"
  "</html>");
  char htmlCharArray[htmlPageSettings.length()];
  htmlPageSettings.toCharArray(htmlCharArray, htmlPageSettings.length());
  server.send ( 200, "text/html", htmlPageSettings );
}

void handleSettingsSave() {
  if (server.arg("pms") != ""){
    wateringPeriodMs = server.arg("pms").toInt();
    SaveEeprom();
    updateTimersPeriods();
  }
  if (server.arg("dms") != ""){
    wateringDurationMs = server.arg("dms").toInt();
    SaveEeprom();
    updateTimersPeriods();
  }
  server.send ( 200, "text/html", "Saved" );
}

void startWatering(){
  waterOn = true;
  openRelay();
  timerWateringDuration.start();
}

void stopWatering(){
  waterOn = false;
  closeRelay();
  timerWateringDuration.stop();
}

void startTestWatering(int testPeriodMs){
  waterOn = true;
  openRelay();
  timerTestWateringDuration.interval(testPeriodMs);
  timerTestWateringDuration.start();
}

void stopTestWatering(){
  waterOn = false;
  closeRelay();
  timerTestWateringDuration.stop();
}

String getTimeStringFromMs(int millisIn){
  int secsIn = millisIn / 1000;
  int millis = millisIn - secsIn * 1000;
  int seconds = secsIn%60;
  int minutes = (secsIn/60)%60;
  int hours = (secsIn/3600)%24;
  int days = (secsIn/86400);
  String res = String();

  if(days > 0) res += String(days) + "days ";
  if(hours > 0) res += String(hours) + "hours ";
  if(minutes > 0) res += String(minutes) + "minutes ";
  if(seconds > 0) res += String(seconds) + "seconds ";
  if(millis > 0) res += String(millis) + "milliseconds ";
  return res;
}

void updateTimersPeriods(){
  timerWateringPeriod.stop();
  timerWateringPeriod.interval(wateringPeriodMs);
  timerWateringPeriod.start();
  timerWateringDuration.stop();
  timerWateringDuration.interval(wateringDurationMs);
}
